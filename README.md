# What do I click on?

Start by downloading 'Workshop_CellProfilerToR.nb.html' to your local machine and opening it in a web browser.

After that, try downloading this entire repository (or cloning it if you're familiar with Git) and then exploring the CellProfiler pipeline 'ExampleHuman.cpproj' and the R Notebook 'Workshop_CellProfilerToR.Rmd'

## More notes

This repository contains the supporting files for the workshop *Analysing High-Content Analysis Data from CellProfiler Using R Programming Language*, written by Stuart Higgins (stuart.higgins@imperial.ac.uk).

This uses a modified version of the ['Example Human cells' pipeline](https://cellprofiler.org/examples), provided by the CellProfiler team. That example uses images of human cells, reproduced here under a CC-0 license. Original notice supplied with the example file:

"The images of human cells were provided by Jason Moffat.

Moffat J, Grueneberg DA, Yang X, Kim SY, Kloepfer AM, Hinkle G, Piqani B, Eisenhaure TM, Luo B, Grenier JK, Carpenter AE, Foo SY, Stewart SA, Stockwell BR, Hacohen N, Hahn WC, Lander ES, Sabatini DM, Root DE (2006) A lentiviral RNAi library for human and mouse genes applied to an arrayed viral high-content screen. Cell, 124(6):1283-98 / doi: 10.1016/j.cell.2006.01.040. PMID 16564017

The images in this example are licensed as CC-0."